<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use function GuzzleHttp\json_decode;
use Illuminate\Support\Carbon;
use function PHPUnit\Framework\throwException;
use JsonException;
use function PHPUnit\Framework\returnCallback;

class ApiController extends Controller
{
    
    protected static $client;
    protected static $uri = 'https://youtube.googleapis.com/youtube/v3/';
    protected static $key = 'AIzaSyCXYlx_ScUQ0iQeR-TFitP4P8s3Vgy-c3g';
    
    
    public static function auth()
    {
        
        self::$client = new Client(['http_errors' => false,'base_uri' => self::$uri]);        
        
    }
    
    public static function get(Request $request, $params=[])
    {
        self::auth();
        
        try{
            $param    = self::Parametros($request->getQueryString());
            
            if(!is_array($param))
            {
                throw new \JsonException($param);
            }
            
            $resposta = self::$client->request('GET','search',['query' => $param]);
            $return = json_decode($resposta->getBody()->getContents(),true);
            
            if($return['pageInfo']['totalResults'] == 0)
            {
                throw new \JsonException("Nenhum resultado encontrado");
            }
            
            $JsonReturn = new TratamentoJsonController();            
            $return = $JsonReturn->JsonReturn($return['items'],$param);
            
        }catch(\JsonException $e ){
            return response()->json(['status' => false, 'message'=> $e->getMessage(), 'total' => 0, 'total' => 0],'200',['X-Header-One' => 'Header Value']);
            
        }
        
        return $return;
    }
    
    
    public static function getid($id)
    {
        self::auth();
        
         try{
            
             $resposta = self::$client->request('GET','videos',['query' => ['part' => 'snippet' , 'id' => $id , 'key' => self::$key]]);
            //'id' => 'k12Q41hpMNE'
            $return[0] = json_decode($resposta->getBody()->getContents(),true);
            
            if($return[0]['pageInfo']['totalResults'] == 0)
            {
                throw new \JsonException("Nenhum resultado encontrado mesmo");
            }
            
            $JsonReturn = new TratamentoJsonController();
            $return = $JsonReturn->JsonReturnId($return);
            
        }catch(\JsonException $e ){
            return response()->json(['status' => false, 'message'=> $e->getMessage(), 'total' => 0, 'total' => 0],'200',['X-Header-One' => 'Header Value']);
            
        }
        return $return;
        
    }
    
    public static function Parametros($param)
    {
        $outputParam = '';
        parse_str($param, $outputParam);  
        $Param = ['published_at','id','title','description','thumbnail'];        
        $thumbnail = ['all','default', 'medium','high'];
            
        $ParamReturn = ['part' => 'snippet','maxResults' => '10','key' => self::$key];
        try{
            foreach($outputParam as $k => $v)
            {
                
                switch ($k) {
                    case 'published_at':
                        
                        $date = explode('-',$v);
                        if(checkdate($date[1], $date[2],$date[0]))
                        {
                            if(isset($outputParam['hour'])){
                                $dtHr = substr($outputParam['hour'], 0, 5);
                                
                                if (preg_match('/^[0-9]{2}:[0-9]{2}$/', $dtHr)) {
                                    $horas = substr($outputParam['hour'], 0, 2);
                                    $minutos = substr($outputParam['hour'], 3, 2);
                                    
                                    if (($horas > "23") || ($minutos > "59")) {
                                        throw new \JsonException(utf8_encode('hora informada inv�lida!'));
                                    }
                                }
                            }else{
                                throw new \JsonException(utf8_encode('hora n�o informada!'));
                            }
                            
                        }else{
                            throw new \JsonException(utf8_encode('Data informada inv�lida! Seu formato deve ser YYYY-MM-DD !'));
                        }
                        
                        $dt = Carbon::createFromFormat('Y-m-d H:i:s', $v.' '.$outputParam['hour']);
                        
                        $data = substr($dt->toRfc3339String(), 0, 19).'Z';
                        
                        $ParamReturn['publishedAfter'] = $data;
                        $ParamReturn['publishedBefore '] = $data;
                        break;
                    case 'id':
                        $ParamReturn['id'] = $v;
                        break;
                    case 'title':
                        $ParamReturn['q'] = $v;
                        break;
                    case 'description':
                        $ParamReturn['q'] = $v;
                        break;
                    case 'thumbnail':
                        
                        if (in_array( $v, $thumbnail)) {
                            $ParamReturn['thumbnail'] = $v;
                        }else{
                            throw new \JsonException(utf8_encode('Use "default", "medium" ou "high" para visualizar a thumbnail desejada!'));
                        }
                        break;
                        
                }
                
            }
        }catch(\Exception $e ){
            throw new \JsonException(utf8_encode('Erro na execu��o da API'));
        }
       return $ParamReturn;
    }
    
}
