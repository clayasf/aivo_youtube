<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;

class TratamentoJsonController extends Controller
{
    
        
    public function JsonReturnId($array)
    {
        $return = ['status' => true,'message' => 'sucess', 'total' => count($array) ];

        foreach($array as $v)
        {
            
            $return['data']['id'] = $v['items'][0]['id'];
            $return['data']['title'] = $v['items'][0]['snippet']['title'];
            $return['data']['description'] = $v['items'][0]['snippet']['description'];
            $return['data']['thumbnail'] = $v['items'][0]['snippet']['thumbnails'];
            $return['data']['published_at '] = $v['items'][0]['snippet']['publishedAt'];
            $return['data']['URL'] = 'https://www.youtube.com/watch?v='.$v['items'][0]['id'];
        }
     
        return $return;
        
    }
    
    public function JsonReturn($array,$param)
    {
        $return = ['status' => true,'message' => 'sucess', 'total' => count($array) ];
        $thumbnail = (isset($param['thumbnail']) ? $param['thumbnail'] : 'default');
        
        foreach($array as $key => $v)
        {
            
            $return['data'][$key]['id'] = $v['id']['videoId'];
            $return['data'][$key]['title'] = $v['snippet']['title'];
            $return['data'][$key]['description'] = $v['snippet']['description'];
            $return['data'][$key]['thumbnail'] = $v['snippet']['thumbnails'][$thumbnail];
            $return['data'][$key]['published_at '] = $v['snippet']['publishedAt'];
            $return['data'][$key]['URL'] = 'https://www.youtube.com/watch?v='.$v['id']['videoId'];
        }
        
        return $return;
        
    }
    
}
