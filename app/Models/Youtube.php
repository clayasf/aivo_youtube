<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Youtube extends Model
{
    
    protected  $table = 'youtube';
    
    protected $fillable = [
        
        'published_at ',
        'id',
        'title',
        'description',
        'thumbnail',
        'extra',
        
        
        
    ];
    
    protected $casts = [
        'published_at ' => 'Timestamp'
    ];
    
    public  $timestamps = false;
}
