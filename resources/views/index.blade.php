<html>

<head>
    <meta charset="utf-8">
    <title>Aivo | Teste Youtube</title>
    <link href="https://global-uploads.webflow.com/5e2f2362e479664d4e15718a/css/aivo-site.webflow.66eed5bc3.css"rel="stylesheet" type="text/css">    
</head>
<body class="body">
	<div data-collapse="medium" data-animation="default"
		data-duration="400" role="banner" class="navbar-desktop w-nav">
		<div class="div-block-231">
			<div class="container">
				<div class="nav-container">
					<a href="/" aria-current="page"
						class="brand-5 w-nav-brand w--current" aria-label="home"><img
						src="https://global-uploads.webflow.com/5e2f2362e479664d4e15718a/5e5d6f682aab1708a780d663_b-aivo-navlogo.svg"
						alt=""></a>
				</div>
			</div>
			<div class="div-block-356">
				<div class="blog-subscription-form w-form">
					<form id="pesquisa" name="pesquisa" method="post">
						<div class="div-block-504">
							<div class="email-input">
								<label for="email-3" class="field-label">YouTube</label><input
									type="email" class="regular-input w-input" 
									name="email" data-name="email"
									placeholder="Pesquisar no Youtube" id="email" required="">
							</div>
							<div class="submit-container">
								<input type="submit" value="Pesquisar"
									data-wait="Please wait..."
									class="form-submit-button w-button">
							</div>
						</div>
					</form>
					<div class="w-form-done">
						<div>Obrigado! O seu envio foi recebido!</div>
					</div>
					<div class="w-form-fail">
						<div>Oops! Alguma coisa correu mal ao enviar o formulário.</div>
					</div>
				</div>
			</div>			
		</div>
	</div>

	<div class="footer">
		<div class="container">
			<div class="footer-social-blog">
				<div class="flex">
					<div class="div-block-352">
						<div class="div-block-353">
							<p class="footer-col-title">Teste API Youtube</p>
						</div>
					</div>
					<div class="div-block-360">
						<div class="div-block-375">
							<a data-w-id="fb3ad573-33d8-fe5e-cc59-d82fce0ed14c"
								href="https://www.facebook.com/aivo.co/" target="_blank"
								class="social-icon w-inline-block"><div class="div-block-378">
									<img
										src="https://global-uploads.webflow.com/5e2f2362e479664d4e15718a/5efe5114f7afea538deb3053_facebook-icon.png"
										alt="Ícone do Facebook" class="image-37">
								</div></a><a data-w-id="fb3ad573-33d8-fe5e-cc59-d82fce0ed14f"
								href="https://twitter.com/Aivo_en" target="_blank"
								class="social-icon w-inline-block"><div class="div-block-378">
									<img
										src="https://global-uploads.webflow.com/5e2f2362e479664d4e15718a/5efe5115e08dc716ae736e41_twitter-icon.png"
										alt="Ícone do Twitter" class="image-37">
								</div></a><a data-w-id="fb3ad573-33d8-fe5e-cc59-d82fce0ed152"
								href="https://www.linkedin.com/company/aivo-/" target="_blank"
								class="social-icon w-inline-block"><div class="linkedin-icon">
									<img
										src="https://global-uploads.webflow.com/5e2f2362e479664d4e15718a/5efe511528e8d1aa8bfb0328_linkedin-icon.png"
										alt="Ícone LinkedIn" class="image-37">
								</div></a><a data-w-id="fb3ad573-33d8-fe5e-cc59-d82fce0ed155"
								href="https://www.youtube.com/user/aivoco" target="_blank"
								class="social-icon _0-rm w-inline-block"><div
									class="div-block-378">
									<img
										src="https://global-uploads.webflow.com/5e2f2362e479664d4e15718a/5efe5115c33c88cc31bdd1c8_youtube-icon.png"
										alt="Ícone LinkedIn" class="image-37">
								</div></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>